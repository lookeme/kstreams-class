package ru.curs.stateless.configuration;

import lombok.RequiredArgsConstructor;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Produced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.support.serializer.JsonSerde;
import ru.curs.counting.model.Bet;

import static ru.curs.counting.model.TopicNames.BET_TOPIC;
import static ru.curs.counting.model.TopicNames.GAIN_TOPIC;

@Configuration
@RequiredArgsConstructor
public class TopologyConfiguration {

    @Bean
    public Topology createTopology(StreamsBuilder streamsBuilder) {
        KStream<String, Bet> input =
                streamsBuilder.stream(BET_TOPIC, Consumed.with(Serdes.String(), new JsonSerde<>(Bet.class)));

        KStream<String, Long> gain = input.mapValues( v -> Math.round(v.getAmount() * v.getOdds()));
//        return streamsBuilder.build();
        gain.to(GAIN_TOPIC, Produced.with(Serdes.String(), new JsonSerde<>(Long.class)));
        return streamsBuilder.build();
    }
}
