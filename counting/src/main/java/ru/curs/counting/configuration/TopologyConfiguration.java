package ru.curs.counting.configuration;

import lombok.RequiredArgsConstructor;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.support.serializer.JsonSerde;
import ru.curs.counting.model.Bet;

import static ru.curs.counting.model.TopicNames.BET_TOPIC;

@Configuration
@RequiredArgsConstructor
public class TopologyConfiguration {

    @Bean
    public Topology createTopology(StreamsBuilder streamsBuilder) {
        KStream<String, Bet> input = streamsBuilder.
                stream(BET_TOPIC,
                        Consumed.with(Serdes.String(),
                                new JsonSerde<>(Bet.class))
                );
        KStream<String, Long> gain = input.mapValues(v -> Math.round(v.getAmount() * v.getOdds()));
        KTable<String, Long> reduce = gain.groupByKey()
                .reduce(Long::sum, Materialized.with(Serdes.String(), new JsonSerde<>(Long.class)));
        reduce.toStream().to("SHOULDER", Produced.with(Serdes.String(), new JsonSerde<>(Long.class)));
        Topology topology = streamsBuilder.build();
        System.out.println("==============================");
        System.out.println(topology.describe());
        System.out.println("==============================");
        // https://zz85.github.io/kafka-streams-viz/
        return topology;
    }
}
