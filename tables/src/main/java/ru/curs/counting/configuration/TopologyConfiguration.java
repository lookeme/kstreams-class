package ru.curs.counting.configuration;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.support.serializer.JsonSerde;
import ru.curs.counting.Total;
import ru.curs.counting.model.Bet;
import ru.curs.counting.model.EventScore;
import ru.curs.counting.model.Outcome;
import ru.curs.counting.model.Score;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import static ru.curs.counting.model.TopicNames.*;

@Configuration
public class TopologyConfiguration {

    @Bean
    Topology createTopology(StreamsBuilder streamsBuilder) {

        KStream<String, Bet> input = streamsBuilder.
                stream(BET_TOPIC,
                        Consumed.with(Serdes.String(),
                                new JsonSerde<>(Bet.class))
                );

        KTable<String, Long> totalValues = input.groupByKey().aggregate(
                () -> 0L, (k, v, a) -> a + Math.round(v.getAmount() * v.getOdds()),
                Materialized.with(Serdes.String(), new JsonSerde<>(Long.class))
        );


        KTable<String, ru.curs.counting.Total> totals =
                totalValues.mapValues((k, v)-> new Total(k.substring(0, k.lastIndexOf(':')), v),
                        Materialized.with(Serdes.String(), new JsonSerde<>(Total.class)));

        KTable<String, EventScore> eventScores = streamsBuilder.table(EVENT_SCORE_TOPIC,
                Consumed.with(Serdes.String(), new JsonSerde<>(EventScore.class)
                ));

        KTable<String, String> joined = totals.join(eventScores,
                Total::getMatch,
                (total, score) -> String.format("(%s)\t%d", score.getScore(), total.getTotal())
        );


//        KStream<String, EventScore> eventScores = streamsBuilder.stream(EVENT_SCORE_TOPIC,
//                Consumed.with(Serdes.String(), new JsonSerde<>(EventScore.class)
//                ));
//
//        KStream<String, Score> scores = eventScores
//                .flatMap((k, v) ->
//                        Stream.of(Outcome.H, Outcome.A).map(o ->
//                                KeyValue.pair(
//                                        String.format("%s:%s", k, o), v))
//                                .collect(Collectors.toList()))
//                .mapValues(EventScore::getScore);
//
//
//        KTable<String, Score> tableScores = scores
//                .toTable(Materialized.with(Serdes.String(), new JsonSerde<>(Score.class)));
//
//
//        KTable<String, String> joined = totals.outerJoin(tableScores,
//                (total, eventScore) -> String.format("(%s)\t%d", eventScore, total));
//
        joined.toStream().to(SHOULDER_SCORE, Produced.with(Serdes.String(), Serdes.String()));

        Topology topology = streamsBuilder.build();
        System.out.println("========================================");
        System.out.println(topology.describe());
        System.out.println("========================================");
        return topology;
    }
}
