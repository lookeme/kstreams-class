package ru.curs.counting;

import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@Builder
@RequiredArgsConstructor
public class Total {
    private final String match;
    private final long total;
}

