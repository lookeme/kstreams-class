package ru.curs.windows.configuration;

import lombok.RequiredArgsConstructor;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.support.serializer.JsonSerde;
import ru.curs.counting.model.*;
import ru.curs.windows.transformer.ScoreTransformer;

import java.time.Duration;

import static ru.curs.counting.model.TopicNames.*;

@Configuration
@RequiredArgsConstructor
public class TopologyConfiguration {

    @Bean
    public Topology createTopology(StreamsBuilder streamsBuilder) {
        KStream<String, Bet> bets =
                streamsBuilder.stream(BET_TOPIC,
                        Consumed.with(Serdes.String(),
                                new JsonSerde<>(Bet.class)).withTimestampExtractor(
                                        (record, previousTimestamp) -> ((Bet) record.value()).getTimestamp()
                                )
                );


        KStream<String, EventScore> eventScores = streamsBuilder.stream(
                EVENT_SCORE_TOPIC,
                Consumed.with(Serdes.String(), new JsonSerde<>(EventScore.class))
                        .withTimestampExtractor((record, previousTimestamp) ->
                                ((EventScore) record.value()).getTimestamp())
        );


        KStream<String, Bet> winningBet = new ScoreTransformer().transformStream(streamsBuilder, eventScores);

        KStream<String, Fraud> join = bets.join(winningBet,
                (Bet bet, Bet wBet) ->
                        Fraud.builder().bettor(bet.getBettor())
                                .outcome(bet.getOutcome())
                                .amount(bet.getAmount())
                                .match(bet.getMatch())
                                .odds(bet.getOdds())
                                .lag(wBet.getTimestamp() - bet.getTimestamp())
                                .build(),
                JoinWindows.of(Duration.ofSeconds(1)).before(Duration.ZERO),
                StreamJoined.with(Serdes.String(), new JsonSerde<>(Bet.class), new JsonSerde<>(Bet.class))
        );

        join.to(FRAUD_TOPIC, Produced.with(Serdes.String(), new JsonSerde<>(Fraud.class)));
        Topology topology = streamsBuilder.build();
        System.out.println("==========================");
        System.out.println(topology.describe());
        System.out.println("==========================");
        return topology;
    }

}
